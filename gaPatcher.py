#!/usr/bin/python

from bs4 import BeautifulSoup, Tag, Comment
import sys
 
# load the file
def patch(filepath):

    with open(filepath) as inf:
        txt = inf.read()
        soup = BeautifulSoup(txt, 'html.parser')
 
    # create new link
    with open('googleAnalytics.js') as gaFile:
        ga = gaFile.read()
        # insert it into the document
        scrTag = Tag(soup, name = 'script')
        scrTag.string = ga
        soup.head.insert(0, scrTag)
        scrTag.insert_before(Comment('Google Analytics'))
        scrTag.insert_after(Comment('End Google Analytics'))
 
        # save the file again
        with open(filepath, 'w') as outf:
            outf.write(str(soup))


if __name__=='__main__':
    patch(sys.argv[1])
